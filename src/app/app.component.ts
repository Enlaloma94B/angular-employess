import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string= 'App Employs';
  msg: string = '';


  employees = [
  {'name': 'mICHE', position: 'manager', email:'mich@gmail.com'},
  {'name': 'Guillermo', position: 'DBA', email:'guille@gmail.com'},
  {'name': 'julia', position: 'PMO', email: 'juli@gmail.com'}

];
model:any = {};
model2:any = {};
hideUpdate:boolean = true;

addEmployee():void{
  this.employees.push(this.model);
  this.msg = 'Add succesfull'
};

deleteEmployee(i):void{
  var answer =  confirm('are you sure delete?');
  if(answer) {
    this.employees.splice(i, 1);
    this.msg = 'Deleted'
  }
}

myValue;

editEmployee(i):void{
  this.hideUpdate = false;
  this.model2.name = this.employees[i].name;
  this.model2.position = this.employees[i].position;
  this.model2.email = this.employees[i].email;
  this.myValue = i;
};

updateEmployee():void{
  let i =this.myValue;
  for(let j = 0; j < this.employees.length; j++){
    if(i == j) {
      this.employees[i] = this.model2;
      this.msg ='updated'
      this.model2 = {};
    }
  }

};

closeAlert():void{
  this.msg = '';
}


}


